﻿using UnityEngine;

namespace Assets
{
    public class FloorClick : MonoBehaviour
    {
    
        void Start ()
        {
	    
        }
	
        void Update ()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                var floor = new Plane(Vector3.up, Vector3.zero);
                float distance;
                if (floor.Raycast(ray, out distance))
                {
                    var machineGunTower = GameObject.Find("MachineGunTower");
                    Instantiate(machineGunTower, ray.GetPoint(distance), Quaternion.identity);
                }
         
            }

            for (var i = 0; i < Input.touchCount; ++i)
            {
                Touch touch = Input.GetTouch(i);
                if (touch.phase == TouchPhase.Began)
                {
                    var ray = Camera.main.ScreenPointToRay(touch.position);
                    var floor = new Plane(Vector3.up, Vector3.zero);
                    float distance;
                    if (floor.Raycast(ray, out distance))
                    {
                        var machineGunTower = GameObject.Find("MachineGunTower");
                        Instantiate(machineGunTower, ray.GetPoint(distance), Quaternion.identity);
                    }
                }
            }
        }

    
    }
}
