﻿using Assets.Scripts.Towers;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts
{
    public class SniperTower : MonoBehaviour, ITower
    {
        private GameObject _target;
        private float _lastShot = 0f;

        private float Range;
        private float AttackSpeed;
        private SniperTowerStats _stats = new SniperTowerStats();

        public void Register(LevelBuyBehaviour levelBuyBehaviour)
        {
            _stats.Register(levelBuyBehaviour, 30);
        }

        public void Register(DpsDisplay dpsDisplay)
        {
            _stats.Register(dpsDisplay);
        }

        public void Register(UpgradePanelBehaviour upgradePanelBehaviour)
        {
            _stats.Register(upgradePanelBehaviour);
        }



        void Awake()
        {
        }

        void Start()
        {
        }

        void Update()
        {
            if (_target != null && Time.timeSinceLevelLoad > _lastShot + (1/_stats.AttacksPerSecond))
            {
                BulletFactory.Instance().Create(transform.position,_target.transform.position - transform.position, 300f, _stats.Damage());
                _lastShot = Time.timeSinceLevelLoad;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _target = other.gameObject;
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (_target == other)
                _target = null;
        }

        void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _target = other.gameObject;
            }
        }

        public void DeregisterEnemy(Enemy enemy)
        {
            if (_target == enemy)
                _target = null;
        }
    }
}