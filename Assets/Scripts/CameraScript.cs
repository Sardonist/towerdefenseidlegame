﻿using UnityEngine;

namespace Assets.Scripts
{
	public class CameraScript :MonoBehaviour
	{
		public void Start()
		{
			
		}

		public void Update()
		{
			//if (Input.GetAxis("Mouse ScrollWheel") > 0f ) // forward
			if (Input.GetKey(KeyCode.UpArrow) && Camera.main.fieldOfView < 87 )//87
			{
				Camera.main.fieldOfView += 1.0f;
			}
			//else if (Input.GetAxis("Mouse ScrollWheel") < 0f ) // backwards
			else if (Input.GetKey(KeyCode.DownArrow) && Camera.main.fieldOfView > 8) //8
			{
				Camera.main.fieldOfView -= 1.0f;
			}
		}
	}
}

