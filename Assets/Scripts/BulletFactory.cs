﻿using UnityEngine;

namespace Assets.Scripts
{
    public class BulletFactory : MonoBehaviour
    {
        public GameObject BulletPrefab;

        public BulletFactory()
        {
            _instance = this;
        }

        private static BulletFactory _instance;
        public static BulletFactory Instance()
        {
            return _instance;
        }

        public void Create(Vector3 origin, Vector3 direction, float speed, float damage)
        {
            var bullet = (GameObject)GameObject.Instantiate(BulletPrefab, direction, Quaternion.identity);
            bullet.transform.position = origin;
            var bulletBehaviour = bullet.GetComponent<BulletBehaviour>();
            bulletBehaviour.SetDamage(damage);
            Debug.Log(origin + " : " + direction + " : " + speed + " : " + damage);
            GameObject.Destroy(bullet, 1);
            var physicalBullet = bullet.GetComponent<Rigidbody>();
            physicalBullet.AddForce((direction).normalized * speed);
        }
    }
}
