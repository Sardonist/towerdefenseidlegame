﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Assets.Scripts
{
    public class LevelManager
    {
        private static int _currentLevel = 1;

        private static LevelManager _instance;
        private EnemySpawner _spawner;
        private int _aliveEnemies;

        public delegate void Kill();
        public static event Kill OnReset;
        public delegate void LevelChanged(int level);
        public static event LevelChanged OnLevelChange;

        public LevelManager(GameObject spawnPoint)
        {
            _spawner = new EnemySpawner(spawnPoint);
            _spawner.StartSpawning();
            _instance = this;
        }

        public static LevelManager Instance()
        {
            return _instance;
        }

        internal int GetWave()
        {
            return _currentLevel;
        }

        internal void Start()
        {
            StartNextWave();
        }

        public void EnemyKilled()
        {
            _aliveEnemies--;
            if (_aliveEnemies == 0)
            {
                _currentLevel++;
                OnLevelChange(_currentLevel);
                StartNextWave();
            }
        }

        private void StartNextWave()
        {
            
            for (int i = 0; i < 10; i++)
            {
                _spawner.Add(new EnemyConfiguration()
                {
                    Life = 1 * _currentLevel,
                    Speed = 3,
                    GoldOnKill = _currentLevel * 5
                }
                );
                _aliveEnemies++;
            }
        }

        internal void ResetWave()
        {
            _spawner.Clear();
            OnReset();
            _aliveEnemies = 0;
            StartNextWave();
        }
    }

    public class EnemySpawner
    {
        private SpawnPoint _spawnPoint;
        private Queue<EnemyConfiguration> _enemiesToBeSpawned = new Queue<EnemyConfiguration>();

        private float _timeSinceLastSpawn;

        public EnemySpawner(GameObject spawnPoint)
        {
            _spawnPoint = spawnPoint.GetComponent<SpawnPoint>();
        }

        public void StartSpawning()
        {
            new Thread(Run).Start();
        }

        internal void Add(EnemyConfiguration enemyConfiguration)
        {
            _enemiesToBeSpawned.Enqueue(enemyConfiguration);
        }

        void Run()
        {
            while (true)
            {
                if (_enemiesToBeSpawned.Count > 0)
                {
                    _spawnPoint.Spawn(_enemiesToBeSpawned.Dequeue());
                }
                Thread.Sleep(2000);
            }
        }

        internal void Clear()
        {
            _enemiesToBeSpawned.Clear();
        }
    }
}