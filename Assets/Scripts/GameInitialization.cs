﻿using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameInitialization : MonoBehaviour
    {
        public GameObject SpawnPoint;
        public GameObject TowersPanelPrefab;
        public GameObject SniperTowerPrefab;
        public GameObject LaserTowerPrefab;
        public GameObject Canvas;
        private LevelManager _levelManager;


        void Awake()
        {
            _levelManager = new LevelManager(SpawnPoint);

            Towers.Towers.SniperTower = GameObject.Instantiate(SniperTowerPrefab);
            var sniperTower = Towers.Towers.SniperTower.GetComponent<SniperTower>();
            sniperTower.transform.position = new Vector3(1, 1, 0);

            Towers.Towers.LaserTower = GameObject.Instantiate(LaserTowerPrefab);
            var laserTower = Towers.Towers.LaserTower.GetComponent<LaserTower>();
            laserTower.transform.position = new Vector3(-5, 1, 5);

            var towerPanelGameObject = GameObject.Instantiate(TowersPanelPrefab);
            towerPanelGameObject.transform.SetParent(Canvas.transform, false);

            var towerPanelBehaviour = towerPanelGameObject.GetComponent<TowersPanelBehaviour>();
            var sniperTowerPanel = towerPanelBehaviour.CreateNewPanel();

            sniperTower.Register(sniperTowerPanel.DpsDisplay);
            sniperTower.Register(sniperTowerPanel.LevelBuyBehaviour);
            sniperTower.Register(sniperTowerPanel.UpgradePanelBehaviour);

            var laserTowerPanel = towerPanelBehaviour.CreateNewPanel();
            laserTower.Register(laserTowerPanel.DpsDisplay);
            laserTower.Register(laserTowerPanel.LevelBuyBehaviour);
            laserTower.Register(laserTowerPanel.UpgradePanelBehaviour);



        }
        void Start()
        {
            _levelManager.Start();
        }
    }
}
