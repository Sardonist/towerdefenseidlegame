﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{ 
    public class SpawnPoint : MonoBehaviour
    {
        public GameObject EnemyPrefab;
        private Queue<EnemyConfiguration> toBeSpawned = new Queue<EnemyConfiguration>();

        void Update()
        {
            while(toBeSpawned.Count > 0)
            {
                var enemyGameObject = (GameObject)GameObject.Instantiate(EnemyPrefab, transform.position, Quaternion.identity);
                var enemy = enemyGameObject.GetComponent<Enemy>();
                enemy.InitializeWith(toBeSpawned.Dequeue());
            }
        }

        public void Spawn(EnemyConfiguration enemy)
        {
            toBeSpawned.Enqueue(enemy);
        }
    }
}

