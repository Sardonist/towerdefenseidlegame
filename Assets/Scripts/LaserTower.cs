﻿using Assets.Scripts.Towers;
using Assets.Scripts.UI;
using UnityEngine;

namespace Assets.Scripts
{
    public class LaserTower : MonoBehaviour, ITower
    {
        private Enemy _target;
        private LineRenderer _laserRenderer;
        private readonly SniperTowerStats _stats = new SniperTowerStats(); 

        public void Register(LevelBuyBehaviour levelBuyBehaviour)
        {
            _stats.Register(levelBuyBehaviour, 300);
        }

        public void Register(DpsDisplay dpsDisplay)
        {
            _stats.Register(dpsDisplay);
        }

        public void Register(UpgradePanelBehaviour upgradePanelBehaviour)
        {
            _stats.Register(upgradePanelBehaviour);
        }

        void Start()
        {
            _laserRenderer = GetComponent<LineRenderer>();
            EventManager.OnEnemyDeath += DeregisterEnemy;
        }

        void Update()
        {
            if (_target != null)
            {
                _laserRenderer.enabled = true;
                _laserRenderer.SetWidth(0.1f, 0.01f);
                _laserRenderer.SetPosition(0, transform.position);
                _laserRenderer.SetPosition(1, _target.transform.position);
                _target.TakeDamage(1 * Time.deltaTime);
            }
            else
            {
                _laserRenderer.enabled = false;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _target = other.GetComponent<Enemy>();
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (_target == other)
                _target = null;
        }

        void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _target = other.GetComponent<Enemy>();
            }
        }

     
        public void DeregisterEnemy(Enemy enemy)
        {
            if (_target == enemy)
                _target = null;
        }
    }
}