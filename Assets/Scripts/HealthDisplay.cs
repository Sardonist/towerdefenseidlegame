﻿using UnityEngine;

namespace Assets.Scripts
{
    public class HealthDisplay : MonoBehaviour
    {
        private Enemy _enemy;
        private MeshRenderer _renderer;

        void Start()
        {
            _renderer = GetComponent<MeshRenderer>();
            _enemy = transform.parent.gameObject.GetComponent<Enemy>();
        }

        void Update()
        {
            if (_enemy.GetHealthPercentage() < 100)
            {
                _renderer.enabled = true;
                transform.localScale = new Vector3(_enemy.GetHealthPercentage(), 0.2f, 0.2f);
            }
        }
    }
}
