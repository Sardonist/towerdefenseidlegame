﻿namespace Assets.Scripts
{
    public class EventManager
    {
        public delegate void EnemyDeath(Enemy enemy);
        public static event EnemyDeath OnEnemyDeath;

        internal static void EnemyDied(Enemy enemy)
        {
            OnEnemyDeath(enemy);
        }
    }
}