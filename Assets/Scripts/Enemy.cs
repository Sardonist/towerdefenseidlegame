﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


namespace Assets.Scripts
{
    public class Enemy : MonoBehaviour
    {
        private float _speed = 2f;
        private float _originalHealth = 10;
        private float _health = 10;

        private IList<Vector3> _path = new List<Vector3>();
        private int _goldOnKill;

        public void InitializeWith(EnemyConfiguration enemyConfig)
        {
            _health = enemyConfig.Life;
            _originalHealth = enemyConfig.Life;
            _speed = enemyConfig.Speed;
            _goldOnKill = enemyConfig.GoldOnKill;
            LevelManager.OnReset += Kill;
        }

        private void Kill()
        {
            LevelManager.OnReset -= Kill;
            LevelManager.Instance().EnemyKilled();
            Destroy(gameObject);
        }

        void Start()
        {
            _path.Add(new Vector3(-8, 0.5f, -8));
            _path.Add(new Vector3(0, 0.5f, -8));
            _path.Add(new Vector3(0, 0.5f, 0));
            _path.Add(new Vector3(8, 0.5f, 0));
            _path.Add(new Vector3(8, 0.5f, -10));
        }

        void Update()
        {
            if (_path.Count == 0)
            {
                LevelManager.Instance().ResetWave();
                return;
            }
            var destination = _path.First();
            if ((destination - transform.position).magnitude < 0.1f)
                _path.RemoveAt(0);
            var destinationVector = destination - transform.position;
            destinationVector.Normalize();
            transform.position += destinationVector * _speed * Time.deltaTime;
        }

        internal void TakeDamage(float damage)
        {
            _health -= damage;
            if (_health <= 0)
            {
                GameStats.AddGold(_goldOnKill);
                Kill();
            }
        }
        
        public float GetHealthPercentage()
        {
            return _health / _originalHealth;
        }
    }
}