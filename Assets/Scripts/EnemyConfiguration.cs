﻿namespace Assets.Scripts
{
    public class EnemyConfiguration
    {
        public EnemyConfiguration()
        {
        }

        public int GoldOnKill { get; set; }
        public int Life { get; set; }
        public int Speed { get; set; }
    }
}