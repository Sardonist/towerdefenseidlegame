﻿using System;
using Assets.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Towers
{
    public interface ITowerEvents
    {
        event TowerEvents.OnDpsChange OnDpsChanged;
        event TowerEvents.OnLevelChange OnLevelChanged;
    }

    public class TowerEvents
    {
        public delegate void OnLevelChange(int currentLevel);
        public delegate void OnDpsChange(float dps);
    }

    public class SniperTowerStats : ITowerEvents
    {
        private float Range;
        public float AttacksPerSecond = 1;

        public void Register(LevelBuyBehaviour levelBuyBehaviour, int basePrice)
        {
            levelBuyBehaviour.InitializeWith(BuyLevel, basePrice, this);
            OnLevelChanged(_level);
        }

        public void Register(UpgradePanelBehaviour upgradePanelBehaviour)
        {
            upgradePanelBehaviour.Add(BuyDoubleDamage, 100);
            upgradePanelBehaviour.Add(BuyDoubleRange, 1000);
            upgradePanelBehaviour.Add(BuyDoubleAttackSpeed, 10000);
        }

        public void Register(DpsDisplay dpsDisplay)
        {
            dpsDisplay.RegisterWith(this);
            OnDpsChanged(Damage() * AttacksPerSecond);
        }

        private float DamageMultiplier = 1;
        private float BaseDamage = 1;
        private int _level;
        private GameObject _uIPanel;

        public event TowerEvents.OnLevelChange OnLevelChanged;
        public event TowerEvents.OnDpsChange OnDpsChanged;
                
        private void BuyDoubleDamage()
        {
            DamageMultiplier *= 2;
            OnDpsChanged(Damage() * AttacksPerSecond);
        }

        private void BuyDoubleRange()
        {
            Range *= 2;
        }

        private void BuyDoubleAttackSpeed()
        {
            AttacksPerSecond *= 2;
            OnDpsChanged(Damage() * AttacksPerSecond);
        }

        private void BuyLevel()
        {
            _level += 1;
            OnLevelChanged(_level);
            OnDpsChanged(Damage() * AttacksPerSecond);
        }

        public int CurrentLevel()
        {
            return _level;
        }

        public float Damage()
        {
            return (BaseDamage * _level) * DamageMultiplier;
        }
     }
}
