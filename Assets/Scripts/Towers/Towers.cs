﻿using UnityEngine;

namespace Assets.Scripts.Towers
{
    public static class Towers
    {
        public static GameObject SniperTower;
        public static GameObject MachineGunTower;
        public static GameObject LaserTower;
    }
}