﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class GoldDisplay : MonoBehaviour
    {
        private Text textComponent;
        void Start()
        {
            textComponent = GetComponentInChildren<Text>();
            textComponent.text = "Gold: 0";
            GameStats.OnGoldChanged += UpdateGoldDisplay;
        }
        
        void UpdateGoldDisplay(float gold)
        {
            textComponent.text = "Gold: " + gold;
        }
    }
}