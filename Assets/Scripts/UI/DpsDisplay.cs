﻿using Assets.Scripts.Towers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class DpsDisplay : MonoBehaviour {

        public Text Text;
    
        public void RegisterWith(ITowerEvents towerEvents)
        {
            towerEvents.OnDpsChanged += OnDpsChange;
        }

        public void OnDpsChange(float dps)
        {
            Text.text = ((int)dps) + " DPS";
        }
    }
}
