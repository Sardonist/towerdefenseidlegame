﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class TowersPanelBehaviour : MonoBehaviour
    {
        private float currentOffset = 0f;
        public GameObject PanelPrefab;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public TowerPanelBehaviour CreateNewPanel()
        {
            var panel = GameObject.Instantiate(PanelPrefab);
            panel.transform.SetParent(transform, false);
            var anchor = panel.GetComponent<RectTransform>();
            anchor.anchorMin = new Vector2(currentOffset, 0);
            anchor.anchorMax = new Vector2(currentOffset + (1f/3f), 0.5f);
            currentOffset += 1f/3f;
            return panel.GetComponent<TowerPanelBehaviour>();
            
        }

        public class PanelComponents
        {
            public LevelBuyBehaviour LevelBuyBehaviour;
            public DpsDisplay DpsDisplay;
            public UpgradePanelBehaviour UpgradePanelBehaviour;
        }
    }
}
