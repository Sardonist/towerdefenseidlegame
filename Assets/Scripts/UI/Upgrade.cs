﻿using System;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class Upgrade
    {
        private Delegate _activateUpgrade;
        private Button _button;
        private int _requiredGold;
        private bool purchasedYet;

        public Upgrade(Button button, int requiredGold, UnityAction activateUpgrade)
        {
            button.onClick.AddListener(BuyUpgrade);
            _button = button;
            _requiredGold = requiredGold;
            _activateUpgrade = activateUpgrade;
            _button.interactable = false;
            GameStats.OnGoldChanged += UpdateWithGold;
        }

        public void UpdateWithGold(float currentGold)
        {
            lock (_button)
            {
                if (currentGold > _requiredGold && !purchasedYet)
                {
                    _button.interactable = true;
                }
                if (currentGold < _requiredGold)
                {
                    _button.interactable = false;
                }
            }
        }

        public void BuyUpgrade()
        {
            lock (_button)
            {
                if (!purchasedYet)
                {
                    _button.interactable = false;
                    purchasedYet = true;
                    _activateUpgrade.DynamicInvoke();
                }
            }
        }
    }
}