﻿using UnityEngine;

namespace Assets.Scripts.UI
{
    public class TowerPanelBehaviour : MonoBehaviour
    {
        public UpgradePanelBehaviour UpgradePanelBehaviour;
        public LevelBuyBehaviour LevelBuyBehaviour;
        public DpsDisplay DpsDisplay;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
