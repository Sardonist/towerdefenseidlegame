﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class WaveDisplay : MonoBehaviour
    {
        private Text textComponent;

        void Start()
        {
            textComponent = GetComponentInChildren<Text>();
            LevelManager.OnLevelChange += UpdateWaveDisplay;
        }

        private void UpdateWaveDisplay(int level)
        {
            textComponent.text = "Wave " + level;
        }
    }
}