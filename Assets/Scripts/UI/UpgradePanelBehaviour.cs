﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class UpgradePanelBehaviour : MonoBehaviour
    {
        public GameObject UpgradeButton;
        private float currentOffset = 0;
        
        public void Add(UnityAction onClick, int requiredGold)
        {
            var upgradeButton = (GameObject)Instantiate(UpgradeButton, transform.position, Quaternion.identity);
            var rectTransform = upgradeButton.GetComponent<RectTransform>();
            rectTransform.anchorMin = new Vector2(0+(0.15f * currentOffset), 0);
            rectTransform.anchorMax = new Vector2(0.15f + (0.15f * currentOffset), 1);
            currentOffset++;
            new Upgrade(upgradeButton.GetComponent<Button>(), requiredGold, onClick);
            upgradeButton.transform.SetParent(gameObject.transform, false);
        }
    }
}
