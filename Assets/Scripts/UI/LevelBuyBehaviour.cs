﻿using System;
using Assets.Scripts.Towers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class LevelBuyBehaviour : MonoBehaviour
    {
        private Delegate _buyLevel;
        private float _requiredGold;
        public Button Button;
        public Text LevelDisplay;
        public Text CostDisplay;

        internal void InitializeWith(Action buyLevel, int v, ITowerEvents towerEvents)
        {
            Button.interactable = false;
            Button.onClick.AddListener(OnClick);
            _requiredGold = v;
            _buyLevel = buyLevel;
            towerEvents.OnLevelChanged += LevelChanged;
            GameStats.OnGoldChanged += UpdateWithGold;
            CostDisplay.text = ((int)_requiredGold) + " Gold";
            LevelDisplay.text = "Level " + 1;
        }

        public void UpdateWithGold(float currentGold)
        {
            if (currentGold >= _requiredGold)
                Button.interactable = true;
            if (currentGold < _requiredGold)
                Button.interactable = false;
        }

        public void LevelChanged(int currentLevel)
        {
            LevelDisplay.text = "Level " + currentLevel;
        } 

        public void OnClick()
        {
            lock(Button)
            {
                if (_requiredGold < GameStats.Gold)
                {
                    var upgradeCost = _requiredGold;
                    _requiredGold = _requiredGold * 1.3f;
                    GameStats.AddGold(-upgradeCost);
                    _buyLevel.DynamicInvoke();
                    CostDisplay.text = ((int)_requiredGold) + " Gold";
                }
            }
        }
    }
}