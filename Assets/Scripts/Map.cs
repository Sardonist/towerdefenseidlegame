﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class Map : MonoBehaviour
    {
        private List<GameObject> _cubes = new List<GameObject>();

        void Start()
        {
            var xStartingPos = -8.50f;
            var xDistanceBetween = 2f;

            var zStartingPos = 8.63f;
            var zDistanceBetween = -2f;

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    cube.name = i + " " + j;
                    cube.transform.position = new Vector3(xStartingPos + (xDistanceBetween * i), 0.01f, zStartingPos + (zDistanceBetween * j));
                    cube.transform.localScale = new Vector3(2, 0.1f, 2);
                    var cubeRenderer = cube.GetComponent<Renderer>();
                    cubeRenderer.enabled = false;
                    cubeRenderer.material.SetFloat("_Mode", 3);
                    cubeRenderer.material.color = new Color(0.5f, 0.5f, 0.5f, 0.4f);
                    cube.AddComponent<HighlightObject>();
                    _cubes.Add(cube);
                }
            }
        }

        void Update()
        {
			if (Input.GetKeyDown (KeyCode.Q)) 
			{
				SceneManager.LoadScene("Level_02");
			}

        }
    }
}