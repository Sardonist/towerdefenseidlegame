﻿using Assets.Scripts.UI;

namespace Assets.Scripts
{
    public interface ITower
    {
        void Register(LevelBuyBehaviour levelBuyBehaviour);

        void Register(DpsDisplay dpsDisplay);

        void Register(UpgradePanelBehaviour upgradePanelBehaviour);
        void DeregisterEnemy(Enemy enemy);
    }
}
