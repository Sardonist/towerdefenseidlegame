﻿using UnityEngine;

namespace Assets.Scripts
{
    public class HighlightObject : MonoBehaviour
    {
        //private Color startcolor;

        public void Start()
        {
            //startcolor = GetComponent<Renderer>().material.color;
        }

        public void Update()
        {
            bool mouseOver = false;
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            foreach (RaycastHit hit in Physics.RaycastAll(ray))
            {
                if (hit.collider == GetComponent<Collider>())
                {
                    mouseOver = true;
                    break;
                }
            }

            if (mouseOver)
                //GetComponent<Renderer>().material.color = Color.yellow;
                GetComponent<Renderer>().enabled = true;
            else
                //GetComponent<Renderer>().material.color = startcolor;
                GetComponent<Renderer>().enabled = false;
        }
    }
}