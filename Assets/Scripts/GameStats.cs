﻿namespace Assets.Scripts
{
    public static class GameStats
    {
        public static float Gold;
        public delegate void GoldChanged(float  currentGold);
        public static event GoldChanged OnGoldChanged;

        public static void AddGold(float gold)
        {
            Gold += gold;
            OnGoldChanged(Gold);
        }
    }
}
