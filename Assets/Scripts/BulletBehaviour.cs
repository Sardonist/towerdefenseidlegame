﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class BulletBehaviour : MonoBehaviour
    {
        private float _damage;

        //private ITower originat
        // Use this for initialization

        public BulletBehaviour()
        {

        }

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnCollisionEnter(Collision other)
        {
            Destroy(this.gameObject);
            if (other.gameObject.CompareTag("Enemy"))
            {
                other.gameObject.GetComponentInParent<Enemy>().TakeDamage(_damage);
            }
        }

        internal void SetDamage(float damage)
        {
            _damage = damage;
        }
    }
}


